import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import React, { useContext, useState } from 'react';
import LoginScreen from './elements/screens/container/LoginScreen';
import TabScreen from './elements/screens/container/TabScreens';
import ForgotPasswd from './elements/screens/container/ForgotPasswd';
import ContactsScreen from './elements/screens/container/ContactsScreen';
import AddContactsScreen from './elements/screens/container/AddContactsScreen';
const ThemeContext = React.createContext();
const ThemeUpdateContext = React.createContext();
const Language = React.createContext();
const LanguageUpdate = React.createContext();

export function useTheme() {
  return useContext(ThemeContext);
}
export function useThemeUpdate() {
  return useContext(ThemeUpdateContext);
}

export function useLanguage() {
  return useContext(Language);
}
export function useLanguageUpdate() {
  return useContext(LanguageUpdate);
}

const stack = createNativeStackNavigator();

const App = () => {
  const [theme, setTheme] = useState(false);
  const [language, setLanguage] = useState(false);
  const toggleTheme = () => {
    setTheme(!theme);
  };
  const toggleLanguage = () => {
    setLanguage(!language);
  };
  return (
    <ThemeContext.Provider value={theme}>
      <ThemeUpdateContext.Provider value={toggleTheme}>
        <Language.Provider value={language}>
          <LanguageUpdate.Provider value={toggleLanguage}>
            <NavigationContainer>
              <stack.Navigator>
                <stack.Screen
                  name="LoginScreen"
                  component={LoginScreen}
                  options={{ title: '' }}
                />
                <stack.Screen
                  name="TabScreen"
                  component={TabScreen}
                  options={{ title: '' }}
                />
                <stack.Screen
                  name="ContactsScreen"
                  component={ContactsScreen}
                  options={{ title: '' }}
                />
                <stack.Screen
                  name="AddContactsScreen"
                  component={AddContactsScreen}
                  options={{ title: '' }}
                />
                <stack.Screen
                  name="ForgotPasswd"
                  component={ForgotPasswd}
                  options={{ title: '' }}
                />
              </stack.Navigator>
            </NavigationContainer>
          </LanguageUpdate.Provider>
        </Language.Provider>
      </ThemeUpdateContext.Provider>
    </ThemeContext.Provider>
  );
};

export default App;
