export const Color = {
  white: '#FFF',
  black: '#222',
  blue: '#356399',
  lightBlue: '#C6E2FF',
  gray: '#888888',
  green: '#2196F3',
  red: '#FF0000',
};
