import React from 'react-native';
import {
  Image,
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import { vn, en } from './Language';
import { Color } from './Themes';
import { useTheme, useLanguage } from '../../../App';
import { useEffect, useState } from 'react';
import { array } from './List';
type Props = {
  title: string;
  title2: string;
};

type ItemType = {
  name: string;
  email: string;
  image: number;
};

function Item({ title, title2 }: Props) {
  return (
    <View style={styles.allTitle}>
      <Text style={styles.title1}>{title}</Text>
      <Text style={styles.title2}>{title2}</Text>
    </View>
  );
}

const ContactsScreen = ({ navigation, route }: any) => {
  const dataAdd = route.params?.data;

  const renderItem = ({ item }: { item: ItemType }) => {
    return (
      <View style={styles.view1}>
        <View style={styles.view2}>
          <Image source={item.image} style={styles.img} />
        </View>
        <Item title={item.name} title2={item.email} />
      </View>
    );
  };
  const language = useLanguage();
  const UpdateLanguage = language ? en : vn;
  const isTheme = useTheme();
  const UpdateColor = isTheme ? Color.white : Color.black;
  const [dataUser, setDataUser] = useState<any[]>(array);

  useEffect(() => {
    if (dataAdd) {
      setDataUser(dataAdd);
    }
  }, [dataAdd]);

  console.log(dataUser, 'add');

  const addContactsScreen = () => {
    navigation.navigate('AddContactsScreen', { dataUser });
  };
  return (
    <View style={styles.container}>
      <View style={styles.viewTop}>
        <Text style={[styles.textTop, { color: UpdateColor }]}>
          {UpdateLanguage.contacts}
        </Text>
      </View>
      <View style={styles.viewAdd}>
        <View style={styles.tCBOadd}>
          <TouchableOpacity style={styles.tcbo} onPress={addContactsScreen}>
            <Text style={[styles.textAdd, { color: UpdateColor }]}>
              {UpdateLanguage.add}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
      <View style={styles.viewFlatList}>
        <FlatList
          style={styles.flatList}
          data={dataUser}
          renderItem={renderItem}
          keyExtractor={(_, index) => index.toString()}
        />
      </View>
    </View>
  );
};

export default ContactsScreen;
const styles = StyleSheet.create({
  allTitle: {
    height: 60,
    padding: 20,
  },
  container: {
    flex: 1,
  },
  flatList: {
    backgroundColor: Color.white,
  },
  img: {
    borderRadius: 30,
    height: 55,
    width: 55,
  },
  tCBOadd: {
    backgroundColor: Color.white,
    height: 30,
    width: 400,
  },
  tcbo: { width: 200 },
  textAdd: {
    color: Color.blue,
    fontSize: 20,
    left: 30,
  },
  textTop: {
    fontSize: 28,
    fontWeight: 'bold',
    left: 15,
    top: 10,
  },
  title1: {
    fontSize: 18,
    left: -30,
    top: -10,
  },
  title2: {
    color: Color.gray,
    left: -30,
  },
  view1: {
    alignItems: 'center',
    flexDirection: 'row',
    paddingVertical: 13,
  },

  view2: {
    alignItems: 'center',
    borderRadius: 100,
    height: 15,
    justifyContent: 'center',
    width: 90,
  },
  viewAdd: {
    flexDirection: 'row',
    padding: 0,
    top: 0,
  },
  viewFlatList: {
    flex: 4,
  },
  viewTop: {
    backgroundColor: Color.gray,
    flex: 0.6,
  },
});
