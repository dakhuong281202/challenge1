import React, { useState } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Alert,
} from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { vn, en } from './Language';
import { Color } from './Themes';
import { useTheme, useLanguage } from '../../../App';

const LoginScreen = ({ navigation }) => {
  const [email, setEmail] = useState('');
  const [passwd, setPasswd] = useState('');
  const [show, setShow] = useState<boolean>(false);
  const Check = () => {
    if (email === '' && passwd === '') {
      navigation.navigate('TabScreen');
    } else {
      Alert.alert('Login error', 'Incorrect account or password');
    }
  };
  const forgotPasswd = () => {
    navigation.navigate('ForgotPasswd');
  };
  const language = useLanguage();
  const UpdateLanguage = language ? en : vn;
  const isTheme = useTheme();
  const UpdateColor = isTheme ? Color.white : Color.black;
  return (
    <View style={styles.container}>
      <KeyboardAwareScrollView
        style={styles.keyboardAwareScrollView}
        showsVerticalScrollIndicator={false}
      >
        <View style={styles.viewTop}>
          <Text style={[styles.textTop, { color: UpdateColor }]}>
            {UpdateLanguage.signInAccount}
          </Text>
        </View>
        <View style={styles.viewEmail}>
          <View>
            <TextInput
              style={styles.TextInputEmail}
              placeholder={UpdateLanguage.email}
              onChangeText={setEmail}
              value={email}
            />
          </View>
          <View style={styles.viewInputPasswd}>
            <TextInput
              style={styles.TextInputPasswd}
              placeholder={UpdateLanguage.password}
              onChangeText={setPasswd}
              value={passwd}
              secureTextEntry={show ? true : false}
            />
            <TouchableOpacity
              style={styles.TouchableOpacity}
              onPress={() => setShow(!show)}
            >
              <Icon style={styles.icon} name={show ? 'eye-slash' : 'eye'} />
            </TouchableOpacity>
          </View>
          <TouchableOpacity onPress={forgotPasswd}>
            <View style={styles.viewForgot}>
              <Text style={[styles.TextPasswd, { color: UpdateColor }]}>
                {UpdateLanguage.forgot}
              </Text>
            </View>
          </TouchableOpacity>
          <View style={styles.bottomView}>
            <View style={styles.ViewSignIn}>
              <Text
                style={[styles.textSignIn, { color: UpdateColor }]}
                onPress={Check}
              >
                {UpdateLanguage.signIn}
              </Text>
            </View>
          </View>
        </View>
      </KeyboardAwareScrollView>
    </View>
  );
};
const styles = StyleSheet.create({
  TextInputEmail: {
    borderRadius: 10,
    borderWidth: 1,
    height: 70,
    margin: 12,
    padding: 10,
    top: 20,
  },
  TextInputPasswd: {
    borderRadius: 10,
    borderWidth: 1,
    height: 70,
    margin: 12,
    padding: 10,
    width: 370,
  },
  TextPasswd: {
    fontSize: 14,
  },
  TouchableOpacity: {
    top: 15,
  },
  ViewSignIn: {
    alignItems: 'center',
    backgroundColor: Color.blue,
    borderRadius: 10,
    height: 90,
    justifyContent: 'center',
    marginHorizontal: 10,
    top: 50,
    width: 370,
  },

  bottomView: {
    bottom: 30,
  },

  container: {
    backgroundColor: Color.white,
    flex: 1,
  },

  icon: {
    fontSize: 15,
    right: 50,
    top: 25,
  },

  keyboardAwareScrollView: {
    flex: 1,
  },

  textSignIn: {
    color: Color.white,
    fontSize: 20,
    paddingHorizontal: 135,
    paddingVertical: 20,
  },

  textTop: {
    color: Color.black,
    fontSize: 24,
    left: 20,
    top: 30,
  },
  viewEmail: {
    flex: 1,
    marginTop: 40,
  },
  viewForgot: {
    alignItems: 'flex-end',
    height: 40,
    top: 20,
    width: 370,
  },
  viewInputPasswd: {
    flexDirection: 'row',
    height: 70,
    justifyContent: 'space-between',
  },
  viewTop: {
    flex: 0.5,
  },
});

export default LoginScreen;
