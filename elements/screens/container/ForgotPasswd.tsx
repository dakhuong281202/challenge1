import React from 'react';
import {
  View,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import { vn, en } from './Language';
import { Color } from './Themes';
import { useTheme, useLanguage } from '../../../App';

const ForgotPasswd = () => {
  const [value, onChangeText] = React.useState('starling.vn@gmail.com');
  const language = useLanguage();
  const UpdateLanguage = language ? en : vn;
  const isTheme = useTheme();
  const UpdateColor = isTheme ? Color.white : Color.black;
  return (
    <View style={styles.container}>
      <View>
        <Text style={[styles.viewText, { color: UpdateColor }]}>
          {UpdateLanguage.forgot}
        </Text>
      </View>
      <View style={styles.viewInput}>
        <TextInput
          style={styles.textInput}
          onChangeText={(text) => onChangeText(text)}
          value={value}
        />
      </View>

      <TouchableOpacity style={styles.tcbo}>
        <Text style={styles.text}>OK</Text>
      </TouchableOpacity>
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    backgroundColor: Color.white,
    flex: 1,
  },
  tcbo: {
    alignItems: 'center',
    backgroundColor: Color.white,
    borderRadius: 10,
    bottom: 100,
    height: 60,
    justifyContent: 'center',
    marginHorizontal: 20,
    width: 360,
  },
  text: {
    color: Color.white,
    fontSize: 17,
    fontWeight: 'bold',
  },
  textInput: {
    backgroundColor: Color.white,
    borderColor: Color.black,
    borderRadius: 10,
    borderWidth: 1,
    height: 60,
    padding: 10,
    width: 350,
  },
  viewInput: {
    alignItems: 'center',
    flex: 2,
    justifyContent: 'center',
  },
  viewText: {
    fontSize: 25,
    padding: 10,
  },
});

export default ForgotPasswd;
