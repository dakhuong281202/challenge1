import React from 'react-native';
import { Text, StyleSheet, View, TouchableOpacity } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import SettingScreen from './SettingScreen';
import { vn, en } from './Language';
import { useLanguage } from '../../../App';
import ContactsScreen from './ContactsScreen';
import { Color } from './Themes';

const Tab = createBottomTabNavigator();

function MyTabBar({ state, descriptors, navigation }) {
  return (
    <View style={styles.Container}>
      {state.routes.map((route, index) => {
        const { options } = descriptors[route.key];
        const label =
          options.tabBarLabel !== undefined
            ? options.tabBarLabel
            : options.title !== undefined
            ? options.title
            : route.name;

        const isFocused = state.index === index;

        const onPress = () => {
          const event = navigation.emit({
            type: 'tabPress',
            target: route.key,
            canPreventDefault: true,
          });

          if (!isFocused && !event.defaultPrevented) {
            navigation.navigate({ name: route.name, merge: true });
          }
        };

        const onLongPress = () => {
          navigation.emit({
            type: 'tabLongPress',
            target: route.key,
          });
        };

        return (
          <TouchableOpacity
            accessibilityRole="button"
            accessibilityState={isFocused ? { selected: true } : {}}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={onPress}
            onLongPress={onLongPress}
            style={styles.Text}
          >
            <Text style={{ color: isFocused ? Color.gray : Color.blue }}>
              {label}
            </Text>
          </TouchableOpacity>
        );
      })}
    </View>
  );
}

const TabScreen = () => {
  const language = useLanguage();
  const UpdateLanguage = language ? en : vn;
  return (
    <Tab.Navigator tabBar={(props) => <MyTabBar {...props} />}>
      <Tab.Screen
        name={UpdateLanguage.contacts}
        component={ContactsScreen}
        options={{ headerShown: false }}
      />
      <Tab.Screen
        name={UpdateLanguage.settings}
        component={SettingScreen}
        options={{ headerShown: false }}
      />
    </Tab.Navigator>
  );
};
const styles = StyleSheet.create({
  Container: {
    flexDirection: 'row',
  },
  Text: {
    alignItems: 'center',
    flex: 1,
    height: 60,
    justifyContent: 'center',
  },
});

export default TabScreen;
