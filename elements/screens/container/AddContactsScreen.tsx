import React, { useState, useEffect } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  TextInput,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import * as ImagePicker from 'expo-image-picker';
import * as Location from 'expo-location';
import { vn, en } from './Language';
import { Color } from './Themes';
import { useTheme, useLanguage } from '../../../App';

const AddContactsScreen = ({ route }: any) => {
  const data = route.params?.dataUser;

  const [location, setLocation] = useState<Location.LocationObject>();
  const [image, setImage] = useState('');
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [phone, setPhone] = useState('');

  useEffect(() => {
    (async () => {
      let { status } = await Location.requestForegroundPermissionsAsync();
      if (status !== 'granted') {
        return;
      }

      let location = await Location.getCurrentPositionAsync({});
      setLocation(location);
    })();
  }, []);

  let openImagePickerAsync = async () => {
    let pickerResult = await ImagePicker.launchImageLibraryAsync();
    if (!pickerResult.cancelled) {
      setImage(pickerResult.uri);
    }
  };
  const onSubmit = () => {
    data.push({ name, email, image });
    setEmail('');
    setImage('');
    setName('');
    setPhone('');
  };

  const language = useLanguage();
  const UpdateLanguage = language ? en : vn;
  const isTheme = useTheme();
  const UpdateColor = isTheme ? Color.white : Color.black;
  return (
    <View style={styles.container}>
      <KeyboardAwareScrollView
        style={styles.keyboard}
        showsVerticalScrollIndicator={false}
      >
        <View style={styles.viewTop}>
          <Text style={styles.textAdd}>{UpdateLanguage.edit}</Text>
          <View style={styles.viewTCBO}>
            <TouchableOpacity
              onPress={openImagePickerAsync}
              style={styles.tCBO}
            >
              <Text>
                <Icon name="pen" size={15} color={'#ffff'} />
              </Text>
            </TouchableOpacity>
            {image && <Image source={{ uri: image }} style={styles.img} />}
          </View>
        </View>

        <View style={styles.viewAdd}>
          <View style={styles.viewFullName}>
            <View>
              <Text style={[styles.textFullName, { color: UpdateColor }]}>
                {UpdateLanguage.name}
              </Text>
            </View>
            <View style={styles.viewTextInputFullName}>
              <TextInput
                value={name}
                onChangeText={setName}
                placeholder=""
                style={styles.textInputFullName}
              />
            </View>
          </View>

          <View style={styles.viewPhone1}>
            <View style={styles.viewPhone2}>
              <Text style={[styles.textPhone, { color: UpdateColor }]}>
                {UpdateLanguage.phone}
              </Text>
            </View>
            <View style={styles.viewInputPhone}>
              <TextInput
                value={phone}
                onChangeText={setPhone}
                placeholder=""
                style={styles.textInputPhone}
              />
            </View>
          </View>

          <View style={styles.viewNickname1}>
            <View style={styles.viewNickname2}>
              <Text style={[styles.textNickName, { color: UpdateColor }]}>
                {UpdateLanguage.nick}
              </Text>
            </View>

            <View style={styles.ViewIcon}>
              <Icon
                name="bullseye"
                size={15}
                color={Color.black}
                style={styles.icon}
              />
              <TextInput
                value={email}
                onChangeText={setEmail}
                placeholder=""
                style={styles.textInputNickName}
              />
            </View>
          </View>

          <View style={styles.viewLocation2}>
            <View>
              <Text style={[styles.textLocation, { color: UpdateColor }]}>
                {UpdateLanguage.location}
              </Text>
            </View>

            <View>
              <TextInput style={styles.textAddLocation}>
                {location ? location.coords.latitude : ''}
                {location ? location.coords.longitude : ''}
              </TextInput>
            </View>
          </View>

          <TouchableOpacity style={styles.viewBtnAdd} onPress={onSubmit}>
            <View style={styles.viewTextAdd}>
              <Text style={[styles.textBtnAdd, { color: UpdateColor }]}>
                {UpdateLanguage.adds}
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      </KeyboardAwareScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  ViewIcon: {
    borderColor: Color.black,
    borderRadius: 10,
    borderWidth: 1,
    flexDirection: 'row',
    height: 60,
    top: 5,
    width: 350,
  },
  container: {
    backgroundColor: Color.white,
    flex: 1,
  },
  icon: {
    left: 10,
    top: 22,
  },
  img: {
    borderRadius: 50,
    height: 100,
    left: 20,
    width: 100,
  },

  keyboard: {
    flexGrow: 1,
  },
  tCBO: {
    alignItems: 'center',
    backgroundColor: Color.blue,
    borderRadius: 40,
    height: 50,
    justifyContent: 'center',
    left: 80,
    position: 'absolute',
    top: 0,
    width: 50,
    zIndex: 1,
  },
  textAdd: {
    fontSize: 20,
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
  textAddLocation: {
    borderColor: Color.black,
    borderRadius: 10,
    borderWidth: 1,
    height: 60,
    marginTop: 5,
    padding: 10,
    width: 350,
  },
  textBtnAdd: {
    color: Color.black,
    fontSize: 17,
  },
  textFullName: {
    color: Color.black,
  },
  textInputFullName: {
    borderColor: Color.black,
    borderRadius: 10,
    borderWidth: 1,
    height: 60,
    padding: 10,
    top: 5,
    width: 350,
  },
  textInputNickName: {
    height: 60,
    padding: 20,
    top: 0,
    width: 350,
  },
  textInputPhone: {
    borderColor: Color.black,
    borderRadius: 10,
    borderWidth: 1,
    height: 60,
    padding: 10,
    top: 5,
    width: 350,
  },
  textLocation: {
    color: Color.black,
  },
  textNickName: {
    color: Color.black,
  },
  textPhone: {
    color: Color.black,
  },
  viewAdd: {
    flex: 2,
    top: 40,
  },
  viewBtnAdd: {
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
    marginHorizontal: 20,
    marginVertical: 90,
  },
  viewFullName: {
    marginLeft: 20,
  },
  viewInputPhone: {},
  viewLocation2: {
    marginLeft: 20,
    marginTop: 90,
  },
  viewNickname1: { marginLeft: 20, top: 50 },
  viewNickname2: {},
  viewPhone1: {
    marginLeft: 20,
    top: 20,
  },
  viewPhone2: {},
  viewTCBO: {
    alignItems: 'flex-start',
    justifyContent: 'center',
    width: 170,
  },
  viewTextAdd: {
    alignItems: 'center',
    backgroundColor: Color.blue,
    borderRadius: 10,
    borderWidth: 1,
    height: 50,
    justifyContent: 'center',
    top: 10,
    width: 150,
  },
  viewTextInputFullName: {},
  viewTop: {
    flex: 1,
  },
});

export default AddContactsScreen;
