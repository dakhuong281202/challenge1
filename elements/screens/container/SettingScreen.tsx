import React, { useState } from 'react';
import {
  useTheme,
  useThemeUpdate,
  useLanguage,
  useLanguageUpdate,
} from '../../../App';
import { Color } from './Themes';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Switch,
  Alert,
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Modal from 'react-native-modal';
import { en, vn } from './Language';

const SettingScreen = ({ navigation }) => {
  const LoginScreen = () => {
    navigation.navigate('LoginScreen');
  };
  const createTwoButtonAlert = () =>
    Alert.alert(UpdateLanguage.signOut, UpdateLanguage.are, [
      {
        text: 'Cancel',
        onPress: () => console.log('Cancel Pressed'),
      },
      { text: 'OK', onPress: () => LoginScreen() },
    ]);

  const toggleSwitch = () => setIsEnabled((previousState) => !previousState);

  const [isEnabled, setIsEnabled] = useState(false);

  const [isModalVisible, setModalVisible] = useState(false);
  const toggleModal = () => {
    setModalVisible(!isModalVisible);
  };

  const toggleTheme: any = useThemeUpdate();
  const isTheme = useTheme();
  const UpdateColor = isTheme ? Color.white : Color.black;

  const language = useLanguage();
  const UpdateLanguage = language ? en : vn;
  const toggleLanguage: any = useLanguageUpdate();
  return (
    <View style={styles.container}>
      <View style={styles.viewTextSetting}>
        <Text style={[styles.textSetting, { color: UpdateColor }]}>
          {UpdateLanguage.settings}
        </Text>
      </View>

      <View style={styles.viewLanguage}>
        <TouchableOpacity
          style={styles.touchableOpacityLanguage}
          onPress={toggleModal}
        >
          <View style={styles.viewAllLanguage}>
            <View style={styles.iconLanguage}>
              <Icon size={15} name="cog" color={'#2539FF'} />
            </View>
            <Text style={[styles.textLanguage, { color: UpdateColor }]}>
              {UpdateLanguage.settings}
            </Text>
            <Modal
              isVisible={isModalVisible}
              style={styles.viewModal}
              onBackdropPress={toggleModal}
            >
              <View style={styles.viewAllModal}>
                <View style={styles.view}>
                  <TouchableOpacity
                    style={styles.touchableOpacityVietNam}
                    onPress={language === true ? toggleLanguage : toggleModal}
                  >
                    <Text style={styles.textTiengViet}>Tiếng Việt</Text>
                  </TouchableOpacity>

                  <TouchableOpacity
                    style={styles.touchableOpacityEnglish}
                    onPress={language === false ? toggleLanguage : toggleModal}
                  >
                    <Text style={styles.textEnglish}>English</Text>
                  </TouchableOpacity>
                </View>

                <TouchableOpacity
                  style={styles.TouchableOpacityCancel}
                  onPress={toggleModal}
                >
                  <Text style={styles.textCancel}>Cancel</Text>
                </TouchableOpacity>
              </View>
            </Modal>

            <View style={styles.viewShow}>
              <Text style={[styles.textShow, { color: UpdateColor }]}>
                {UpdateLanguage.language}
              </Text>
            </View>
          </View>
        </TouchableOpacity>
      </View>

      <View style={styles.viewTheme}>
        <TouchableOpacity
          style={styles.touchableOpacityTheme}
          onPress={toggleTheme}
        >
          <View style={styles.viewAllTheme}>
            <View style={styles.iconTheme}>
              <Icon size={15} name="cog" color={'#2539FF'} />
            </View>

            <Text style={[styles.textTheme, { color: UpdateColor }]}>
              {UpdateLanguage.themes}
            </Text>

            <View style={styles.viewShowTheme}>
              <Text style={[styles.textShowTheme, { color: UpdateColor }]}>
                {!isTheme ? UpdateLanguage.light : UpdateLanguage.dark}
              </Text>
            </View>
          </View>
        </TouchableOpacity>
      </View>

      <View style={styles.viewNotifications}>
        <View style={styles.viewAllNotifications}>
          <View style={styles.iconNotifications}>
            <Icon size={15} name="cog" color={'#2539FF'} />
          </View>
          <Text style={[styles.textNotifications, { color: UpdateColor }]}>
            {UpdateLanguage.notifications}
          </Text>
          <View style={styles.switch}>
            <Switch
              style={styles.switchText}
              onValueChange={toggleSwitch}
              value={isEnabled}
            />
          </View>
        </View>
      </View>

      <View style={styles.vewSighOut}>
        <TouchableOpacity
          style={styles.touchableOpacitySighOut}
          onPress={createTwoButtonAlert}
        >
          <View>
            <Text style={[styles.textSighOut, { color: UpdateColor }]}>
              {UpdateLanguage.signOut}
            </Text>
          </View>
        </TouchableOpacity>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  TouchableOpacityCancel: {
    alignItems: 'center',
    backgroundColor: Color.white,
    borderRadius: 10,
    height: 50,
    justifyContent: 'center',
    marginTop: 10,
  },
  container: {
    backgroundColor: Color.white,
    flex: 1,
  },
  iconLanguage: {
    left: 10,
    top: 13,
  },
  iconNotifications: {
    left: 10,
    top: 17,
  },
  iconTheme: {
    left: 10,
    top: 13,
  },
  switch: {
    left: 20,
    marginHorizontal: 130,
  },

  switchText: {},
  textCancel: {
    color: Color.red,
    fontSize: 17,
  },
  textEnglish: {
    color: Color.blue,
    fontSize: 17,
  },
  textLanguage: {
    fontSize: 20,
    height: 50,
    marginHorizontal: 20,
    marginVertical: 7,
  },
  textNotifications: {
    fontSize: 20,
    marginHorizontal: 20,
    marginVertical: 10,
  },
  textSetting: {
    fontSize: 25,
    left: 10,
    top: 10,
  },
  textShow: {
    fontSize: 17,
  },
  textShowTheme: {
    fontSize: 18,
  },
  textSighOut: {
    fontSize: 20,
    height: 30,
    left: 20,
  },

  textTheme: {
    fontSize: 20,
    height: 50,
    justifyContent: 'center',
    left: 20,
    top: 5,
    width: 350,
  },
  textTiengViet: {
    color: Color.blue,
    fontSize: 17,
  },
  touchableOpacityEnglish: {
    alignItems: 'center',
    height: 50,
    justifyContent: 'center',
    width: 350,
  },
  touchableOpacityLanguage: {
    backgroundColor: Color.white,
    height: 70,
    justifyContent: 'center',
    top: 30,
  },
  touchableOpacitySighOut: {},
  touchableOpacityTheme: {
    height: 40,
  },
  touchableOpacityVietNam: {
    alignItems: 'center',
    height: 50,
    justifyContent: 'center',
    width: 350,
  },
  vewSighOut: {
    backgroundColor: Color.white,
    height: 70,
    justifyContent: 'center',
    top: 70,
  },
  view: {
    backgroundColor: Color.white,
    borderRadius: 10,
    height: 110,
  },
  // view2: {
  //   borderRadius: 10,
  //   margin: 12,
  // },
  viewAllLanguage: {
    flexDirection: 'row',
    height: 40,
  },
  viewAllModal: {},
  viewAllNotifications: {
    flexDirection: 'row',
    height: 50,
    width: 370,
  },
  viewAllTheme: {
    flexDirection: 'row',
  },
  viewLanguage: {},
  viewModal: {
    justifyContent: 'flex-end',
    marginHorizontal: 20,
  },
  viewNotifications: {
    backgroundColor: Color.white,
    height: 70,
    justifyContent: 'center',
    top: 40,
  },
  viewShow: {
    left: 165,
    top: 10,
  },
  viewShowTheme: {
    right: 50,
    top: 5,
  },
  viewTextSetting: {
    backgroundColor: Color.white,
    height: 80,
    width: '100%',
  },
  viewTheme: {
    backgroundColor: Color.white,
    height: 70,
    justifyContent: 'center',
    top: 35,
  },
});
export default SettingScreen;
